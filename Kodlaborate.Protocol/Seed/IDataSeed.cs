﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.Protocol.Seed {
    public interface IDataSeed<TEntity> {

        IEnumerable<TEntity> GetSeed();

    }
}
