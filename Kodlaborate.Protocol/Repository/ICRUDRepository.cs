﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.Protocol.Repository {

    public interface ICRUDRepository<TEntity> : ISelectRepository<TEntity>
        where TEntity : class {

        long Insert(TEntity entity, Action<StringBuilder> fallback = null);
        IEnumerable<TEntity> InsertRange(IEnumerable<TEntity> entities, Action<StringBuilder> fallback = null);
        long Update(TEntity entity, Action<StringBuilder> fallback = null);
        long Delete(long id, Action<StringBuilder> fallback = null);
        Task<long> InsertAsync(TEntity entity, Action<StringBuilder> fallback = null);
        Task<long> UpdateAsync(TEntity entity, Action<StringBuilder> fallback = null);
        Task<long> DeleteAsync(long id, Action<StringBuilder> fallback = null);
        ICRUDRepository<TEntity> Wipe(Action<StringBuilder> fallback = null);
        Task<ICRUDRepository<TEntity>> WipeAsync(Action<StringBuilder> fallback = null);
        ICRUDRepository<TEntity> WipeDatabase(Action<StringBuilder> fallback = null);
    }

}

