﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.Protocol.Repository {

    public interface ISelectRepository<TEntity> : IBasicRepository<TEntity>
        where TEntity : class {

        IEnumerable<TEntity> SelectBy(Expression<Func<TEntity, bool>> query, Action<StringBuilder> fallback = null);
        IEnumerable<TEntity> Select(Action<StringBuilder> fallback = null);
        TEntity SelectById(long id, Action<StringBuilder> fallback = null);
        Task<TEntity> SelectByIdAsync(long id, Action<StringBuilder> fallback = null);
        TEntity SelectSingleBy(Func<TEntity, bool> query, Action<StringBuilder> fallback = null);
        Task<TEntity> SelectSingleByAsync(Expression<Func<TEntity, bool>> query, Action<StringBuilder> fallback = null);

    }

}

