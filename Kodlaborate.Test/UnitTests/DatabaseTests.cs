﻿using System;
using Kodlaborate.Dependency.Core;
using Kodlaborate.Protocol.Repository;
using Kodlaborate.Database.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Diagnostics;
using Kodlaborate.Seed.Core;

namespace Kodlaborate.Test.UnitTests {

    [TestClass]
    public class DatabaseTests {

        [TestMethod]
        public void DatabaseSeeded() {

            SDataSeeder.SeedAll(KLKernel.Core(), sb => {
                Assert.Fail(sb.ToString());
            });
            var repo = KLKernel.Get<ISelectRepository<Item>>();
            var all = repo.Select();
            Assert.AreNotEqual(0, all.Count());

        }

    }

}
