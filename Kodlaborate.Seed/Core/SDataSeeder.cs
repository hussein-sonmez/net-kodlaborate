﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kodlaborate.Database.Model;
using Kodlaborate.Database.Repository;
using Kodlaborate.Protocol.Repository;
using Kodlaborate.Protocol.Seed;
using Ninject;

namespace Kodlaborate.Seed.Core {
    public static class SDataSeeder {

        public static void SeedAll(IKernel kernel, Action<StringBuilder> fallback) {


            var seeder = kernel.Get<IDataSeed<Comment>>();
            var repo = kernel.Get<ICRUDRepository<Comment>>();
            repo.WipeDatabase(fallback);
            repo.InsertRange(seeder.GetSeed().ToList(), fallback);

        }

    }
}
