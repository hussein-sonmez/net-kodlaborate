﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kodlaborate.Database.Model;
using Kodlaborate.Protocol.Seed;

namespace Kodlaborate.Seed.Core {

    internal class ItemSeed : IDataSeed<Item> {

        private readonly IDataSeed<Language> langSeed;
        private readonly IDataSeed<Project> projSeed;

        public ItemSeed(IDataSeed<Language> langSeed, IDataSeed<Project> projSeed) {
            this.langSeed = langSeed;
            this.projSeed = projSeed;
        }

        public IEnumerable<Item> GetSeed() {

            var langs = this.langSeed.GetSeed();
            var projs = this.projSeed.GetSeed();
            var proj = projs.First();

            for (int i = 0; i < langs.Count(); i++) {
                var lang = langs.ElementAt(i);
                yield return new Item() { Language = lang, Title = "Item-" + i, Project = proj, DirectoryPath = "Path/To/File/"+i, FileName = "File-" + i, InsertedOn = DateTime.Now , UpdatedOn = DateTime.Now };
            }
            
        }

    }

}
