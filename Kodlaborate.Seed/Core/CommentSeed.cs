﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kodlaborate.Database.Model;
using Kodlaborate.Protocol.Seed;

namespace Kodlaborate.Seed.Core {

    internal class CommentSeed : IDataSeed<Comment> {

        private readonly IDataSeed<User> userSeed;
        private readonly IDataSeed<Line> lineSeed;

        public CommentSeed(IDataSeed<User> userSeed, IDataSeed<Line> lineSeed) {
            this.userSeed = userSeed;
            this.lineSeed = lineSeed;
        }

        public IEnumerable<Comment> GetSeed() {

            var users = this.userSeed.GetSeed();
            var lines = this.lineSeed.GetSeed();

            for (int i = 0; i < users.Count(); i++) {
                var user = users.ElementAt(i);
                var line = lines.ElementAt(i);
                yield return new Comment() { Line = line, User = user, State = 1, InsertedOn = DateTime.Now , UpdatedOn = DateTime.Now };
            }
            
        }

    }

}
