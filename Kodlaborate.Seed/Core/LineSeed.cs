﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kodlaborate.Database.Model;
using Kodlaborate.Protocol.Seed;

namespace Kodlaborate.Seed.Core {

    internal class LineSeed : IDataSeed<Line> {

        private readonly IDataSeed<Item> itemSeed;

        public LineSeed(IDataSeed<Item> itemSeed) {
            this.itemSeed = itemSeed;
        }

        public IEnumerable<Line> GetSeed() {

            var items = this.itemSeed.GetSeed();

            for (int i = 0; i < items.Count(); i++) {
                var item = items.ElementAt(i);
                yield return new Line() { Item = item, Content = "Content - " + i, LineNumber = i + 1, InsertedOn = DateTime.Now, UpdatedOn = DateTime.Now };
            }
            
        }

    }

}
