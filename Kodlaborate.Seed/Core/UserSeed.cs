﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kodlaborate.Database.Model;
using Kodlaborate.Protocol.Seed;

namespace Kodlaborate.Seed.Core {

    internal class UserSeed : IDataSeed<User> {

        public IEnumerable<User> GetSeed() {

            for (int i = 0; i < 11; i++) {
                yield return new User() { Cipher = "", FullName = "User Full Name - " + i,
                    Identifier = "identifier"+i, RegisteredOn = DateTime.Now,
                    State = 1, Token = "ffssdf3j90mv" + i };
            }
            
        }

    }

}
