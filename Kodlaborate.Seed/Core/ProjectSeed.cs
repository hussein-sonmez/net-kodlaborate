﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kodlaborate.Database.Model;
using Kodlaborate.Protocol.Seed;

namespace Kodlaborate.Seed.Core {

    internal class ProjectSeed : IDataSeed<Project> {

        private readonly IDataSeed<User> userSeed;

        public ProjectSeed(IDataSeed<User> userSeed) {
            this.userSeed = userSeed;
        }

        public IEnumerable<Project> GetSeed() {

            var users = this.userSeed.GetSeed();

            for (int i = 0; i < users.Count(); i++) {
                var user = users.ElementAt(i);
                yield return new Project() { Title = "Project - " + i, User = user, InsertedOn = DateTime.Now, UpdatedOn = DateTime.Now };
            }
            
        }

    }

}
