﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kodlaborate.Database.Model;

namespace Kodlaborate.API.Models {
    public class ItemNode {

        private static IEnumerable<ItemsView> allViews;

        public static IEnumerable<ItemNode> PopulateTree(IEnumerable<ItemsView> views) {
            allViews = views;
            return allViews.Where(v => !v.ParentItemID.HasValue).ToList().Select(it => new ItemNode(it)).ToList();
        }

        public ItemNode(ItemsView it) {
            this.text = it.ItemTitle;
            this.itemID = String.Format("{0}", it.ItemID);
            var ndli = allViews.Where(v => v.ParentItemID.HasValue && v.ParentItemID.Value == it.ItemID)
                .Select(v => new ItemNode(v));
            if (ndli.Count() > 0) {
                this.nodes = ndli;
                this.selectable = false;
            } else {
                this.selectable = true;
            }
        }

        public bool selectable { get; set; }
        public string text { get; set; }
        public string itemID { get; set; }
        public IEnumerable<ItemNode> nodes { get; set; }
    }
}