﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;
using Kodlaborate.Dependency.Core;
using Kodlaborate.Database.Model;
using Kodlaborate.Protocol.Repository;
using Kodlaborate.API.Models;
using System.Web.Http.Cors;

namespace Kodlaborate.API.Controllers {

    [RoutePrefix("items")]
    [EnableCors("*", "*", "*", PreflightMaxAge = 600)]
    public class ItemsController : ApiController {
        
        ICRUDRepository<ItemsView> itemsDb;
        ICRUDRepository<LinesOfItemsView> linesDb;

        public ItemsController() {
            itemsDb = KLKernel.Get<ICRUDRepository<ItemsView>>();
            linesDb = KLKernel.Get<ICRUDRepository<LinesOfItemsView>>();
        }

        [Route("get")]
        public IQueryable<ItemsView> GetItems() {
            return itemsDb.Select().AsQueryable();
        }

        [Route("{id}/lines")]
        [ResponseType(typeof(LinesOfItemsView))]
        public IHttpActionResult GetLinesOfItem() {

            IHttpRouteData routeData = Request.GetRouteData();
            if (!routeData.Values.ContainsKey("id")) {
                return BadRequest();
            }

            long id = Convert.ToInt64(routeData.Values["id"]);
            return Ok(linesDb.SelectBy(lit => lit.ItemID == id).ToList());

        }

        [Route("tree")]
        [ResponseType(typeof(ItemNode))]
        public IHttpActionResult GetItemsAsTree() {

            return Ok(ItemNode.PopulateTree(itemsDb.Select()));

        }

        // GET: api/Items/5
        [Route("get/{id}")]
        [ResponseType(typeof(ItemsView))]
        public async Task<IHttpActionResult> GetItem() {

            IHttpRouteData routeData = Request.GetRouteData();
            if (!routeData.Values.ContainsKey("id")) {
                return BadRequest();
            }

            long id = Convert.ToInt64(routeData.Values["id"]);
            var itemLanguageView = await itemsDb.SelectSingleByAsync(vw => vw.ItemID == id);
            if (itemLanguageView == null) {
                return NotFound();
            }

            return Ok(itemLanguageView);

        }

        //// PUT: api/Items/5
        //[ResponseType(typeof(void))]
        //public async Task<IHttpActionResult> PutItemLanguageView(long id, ItemLanguageView itemLanguageView) {
        //    if (!ModelState.IsValid) {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != itemLanguageView.ItemID) {
        //        return BadRequest();
        //    }

        //    await db.UpdateAsync(itemLanguageView, sb => {

        //    });

        //    try {
        //        await db.SaveChangesAsync();
        //    } catch (DbUpdateConcurrencyException) {
        //        if (!ItemLanguageViewExists(id)) {
        //            return NotFound();
        //        } else {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// POST: api/Items
        //[ResponseType(typeof(ItemLanguageView))]
        //public async Task<IHttpActionResult> PostItemLanguageView(ItemLanguageView itemLanguageView) {
        //    if (!ModelState.IsValid) {
        //        return BadRequest(ModelState);
        //    }

        //    db.ItemLanguageViews.Add(itemLanguageView);

        //    try {
        //        await db.SaveChangesAsync();
        //    } catch (DbUpdateException) {
        //        if (ItemLanguageViewExists(itemLanguageView.ItemID)) {
        //            return Conflict();
        //        } else {
        //            throw;
        //        }
        //    }

        //    return CreatedAtRoute("DefaultApi", new { id = itemLanguageView.ItemID }, itemLanguageView);
        //}

        //// DELETE: api/Items/5
        //[ResponseType(typeof(ItemLanguageView))]
        //public async Task<IHttpActionResult> DeleteItemLanguageView(long id) {
        //    ItemLanguageView itemLanguageView = await db.ItemLanguageViews.FindAsync(id);
        //    if (itemLanguageView == null) {
        //        return NotFound();
        //    }

        //    db.ItemLanguageViews.Remove(itemLanguageView);
        //    await db.SaveChangesAsync();

        //    return Ok(itemLanguageView);
        //}

        //protected override void Dispose(bool disposing) {
        //    if (disposing) {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        //private bool ItemLanguageViewExists(long id) {
        //    return db.ItemLanguageViews.Count(e => e.ItemID == id) > 0;
        //}

    }

}