﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using InteractivePreGeneratedViews;
using Kodlaborate.Database.Model;

namespace Kodlaborate.API {
    public static class WebApiConfig {
        public static void Register(HttpConfiguration config) {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();
            using (var ctx = new KLEntities()) {
                InteractiveViews.SetViewCacheFactory(ctx, new SqlServerViewCacheFactory(ctx.Database.Connection.ConnectionString));
            }

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);
        }
    }
}
