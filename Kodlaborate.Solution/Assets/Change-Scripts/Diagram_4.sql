/*
   14 Şubat 2016 Pazar01:08:22
   User: 
   Server: DEV-HQ
   Database: klv2db
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Projects
	DROP CONSTRAINT FK_Project_User
GO
ALTER TABLE dbo.Users SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Users', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Users', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Users', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Projects
	(
	ID bigint NOT NULL IDENTITY (1, 1),
	Title nvarchar(255) NOT NULL,
	UserID bigint NOT NULL,
	InsertedOn datetime NOT NULL,
	UpdatedOn datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Projects SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_Projects ON
GO
IF EXISTS(SELECT * FROM dbo.Projects)
	 EXEC('INSERT INTO dbo.Tmp_Projects (ID, Title, UserID)
		SELECT ID, Title, UserID FROM dbo.Projects WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Projects OFF
GO
ALTER TABLE dbo.Containers
	DROP CONSTRAINT FK_Container_Project
GO
DROP TABLE dbo.Projects
GO
EXECUTE sp_rename N'dbo.Tmp_Projects', N'Projects', 'OBJECT' 
GO
ALTER TABLE dbo.Projects ADD CONSTRAINT
	PK_Project PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Projects ADD CONSTRAINT
	FK_Project_User FOREIGN KEY
	(
	UserID
	) REFERENCES dbo.Users
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Projects', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Projects', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Projects', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Containers ADD CONSTRAINT
	FK_Container_Project FOREIGN KEY
	(
	ProjectID
	) REFERENCES dbo.Projects
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Containers SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Containers', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Containers', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Containers', 'Object', 'CONTROL') as Contr_Per 