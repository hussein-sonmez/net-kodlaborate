/*
   15 Şubat 2016 Pazartesi01:48:03
   User: 
   Server: DEV-HQ
   Database: klv2db
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Containers SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Containers', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Containers', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Containers', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Extensions SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Extensions', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Extensions', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Extensions', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Languages SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Languages', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Languages', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Languages', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Items
	(
	ID bigint NOT NULL IDENTITY (1, 1),
	Title nvarchar(250) NOT NULL,
	InsertedOn datetime NOT NULL,
	UpdatedOn datetime NULL,
	ContainerID bigint NOT NULL,
	LanguageID bigint NULL,
	ExtensionID bigint NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Items SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_Items ON
GO
IF EXISTS(SELECT * FROM dbo.Items)
	 EXEC('INSERT INTO dbo.Tmp_Items (ID, Title, InsertedOn, UpdatedOn, ContainerID, LanguageID)
		SELECT ID, Title, InsertedOn, UpdatedOn, ContainerID, LanguageID FROM dbo.Items WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Items OFF
GO
ALTER TABLE dbo.Lines
	DROP CONSTRAINT FK_Line_Item
GO
DROP TABLE dbo.Items
GO
EXECUTE sp_rename N'dbo.Tmp_Items', N'Items', 'OBJECT' 
GO
ALTER TABLE dbo.Items ADD CONSTRAINT
	PK_Item PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Items ADD CONSTRAINT
	FK_Item_Container FOREIGN KEY
	(
	ContainerID
	) REFERENCES dbo.Containers
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Items ADD CONSTRAINT
	FK_Item_Language FOREIGN KEY
	(
	LanguageID
	) REFERENCES dbo.Languages
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Items ADD CONSTRAINT
	FK_Items_Extensions FOREIGN KEY
	(
	ExtensionID
	) REFERENCES dbo.Extensions
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Items', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Items', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Items', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Lines ADD CONSTRAINT
	FK_Line_Item FOREIGN KEY
	(
	ItemID
	) REFERENCES dbo.Items
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Lines SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Lines', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Lines', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Lines', 'Object', 'CONTROL') as Contr_Per 