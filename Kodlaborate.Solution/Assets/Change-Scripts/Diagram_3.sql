/*
   13 Şubat 2016 Cumartesi17:10:09
   User: 
   Server: DEV-HQ
   Database: klv2db
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Item
	DROP CONSTRAINT FK_Item_Container
GO
ALTER TABLE dbo.Container SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Container', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Container', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Container', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Language SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Language', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Language', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Language', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Item
	DROP CONSTRAINT FK_Item_Extension
GO
ALTER TABLE dbo.Extension SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Extension', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Extension', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Extension', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Item
	(
	ID bigint NOT NULL IDENTITY (1, 1),
	Title nvarchar(250) NOT NULL,
	InsertedOn datetime NOT NULL,
	UpdatedOn datetime NULL,
	ContainerID bigint NOT NULL,
	LanguageID bigint NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Item SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_Item ON
GO
IF EXISTS(SELECT * FROM dbo.Item)
	 EXEC('INSERT INTO dbo.Tmp_Item (ID, Title, InsertedOn, UpdatedOn, ContainerID)
		SELECT ID, Title, InsertedOn, UpdatedOn, ContainerID FROM dbo.Item WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Item OFF
GO
ALTER TABLE dbo.Line
	DROP CONSTRAINT FK_Line_Item
GO
DROP TABLE dbo.Item
GO
EXECUTE sp_rename N'dbo.Tmp_Item', N'Item', 'OBJECT' 
GO
ALTER TABLE dbo.Item ADD CONSTRAINT
	PK_Item PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Item ADD CONSTRAINT
	FK_Item_Container FOREIGN KEY
	(
	ContainerID
	) REFERENCES dbo.Container
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Item ADD CONSTRAINT
	FK_Item_Language FOREIGN KEY
	(
	LanguageID
	) REFERENCES dbo.Language
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Item', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Item', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Item', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Line ADD CONSTRAINT
	FK_Line_Item FOREIGN KEY
	(
	ItemID
	) REFERENCES dbo.Item
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Line SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Line', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Line', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Line', 'Object', 'CONTROL') as Contr_Per 