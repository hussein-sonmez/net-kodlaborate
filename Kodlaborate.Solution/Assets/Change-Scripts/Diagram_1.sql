/*
   13 Şubat 2016 Cumartesi16:56:23
   User: 
   Server: DEV-HQ
   Database: klv2db
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Item
	DROP CONSTRAINT FK_Item_Project
GO
ALTER TABLE dbo.Project SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Project', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Project', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Project', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Container
	(
	ID bigint NOT NULL IDENTITY (1, 1),
	Title nvarchar(255) NOT NULL,
	ParentID bigint NULL,
	ProjectID bigint NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Container SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_Container ON
GO
IF EXISTS(SELECT * FROM dbo.Container)
	 EXEC('INSERT INTO dbo.Tmp_Container (ID, Title, ParentID)
		SELECT ID, Title, ParentID FROM dbo.Container WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Container OFF
GO
ALTER TABLE dbo.Container
	DROP CONSTRAINT FK_Container_Container
GO
ALTER TABLE dbo.Item
	DROP CONSTRAINT FK_Item_Container
GO
DROP TABLE dbo.Container
GO
EXECUTE sp_rename N'dbo.Tmp_Container', N'Container', 'OBJECT' 
GO
ALTER TABLE dbo.Container ADD CONSTRAINT
	PK_Container PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Container ADD CONSTRAINT
	FK_Container_Container FOREIGN KEY
	(
	ParentID
	) REFERENCES dbo.Container
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Container ADD CONSTRAINT
	FK_Container_Project FOREIGN KEY
	(
	ProjectID
	) REFERENCES dbo.Project
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Container', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Container', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Container', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Item ADD CONSTRAINT
	FK_Item_Container FOREIGN KEY
	(
	ContainerID
	) REFERENCES dbo.Container
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Item
	DROP COLUMN ProjectID
GO
ALTER TABLE dbo.Item SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Item', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Item', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Item', 'Object', 'CONTROL') as Contr_Per 