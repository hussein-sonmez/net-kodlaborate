/*
   13 Şubat 2016 Cumartesi17:08:17
   User: 
   Server: DEV-HQ
   Database: klv2db
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Language
	(
	ID bigint NOT NULL IDENTITY (1, 1),
	Title nvarchar(128) NOT NULL,
	MimeType nvarchar(128) NOT NULL,
	Mode nvarchar(128) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Language ADD CONSTRAINT
	PK_Language PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Language SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Language', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Language', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Language', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Extension
	(
	ID bigint NOT NULL IDENTITY (1, 1),
	Name nchar(32) NOT NULL,
	LanguageID bigint NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Extension SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_Extension ON
GO
IF EXISTS(SELECT * FROM dbo.Extension)
	 EXEC('INSERT INTO dbo.Tmp_Extension (ID, Name)
		SELECT ID, Name FROM dbo.Extension WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Extension OFF
GO
ALTER TABLE dbo.Item
	DROP CONSTRAINT FK_Item_Extension
GO
DROP TABLE dbo.Extension
GO
EXECUTE sp_rename N'dbo.Tmp_Extension', N'Extension', 'OBJECT' 
GO
ALTER TABLE dbo.Extension ADD CONSTRAINT
	PK_Extension PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Extension ADD CONSTRAINT
	UQ_Extension_Name UNIQUE NONCLUSTERED 
	(
	Name
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Extension ADD CONSTRAINT
	FK_Extension_Language FOREIGN KEY
	(
	LanguageID
	) REFERENCES dbo.Language
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Extension', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Extension', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Extension', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Item ADD CONSTRAINT
	FK_Item_Extension FOREIGN KEY
	(
	ExtensionID
	) REFERENCES dbo.Extension
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Item SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Item', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Item', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Item', 'Object', 'CONTROL') as Contr_Per 