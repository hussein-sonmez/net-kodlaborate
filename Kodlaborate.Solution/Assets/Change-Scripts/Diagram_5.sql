/*
   14 Şubat 2016 Pazar01:23:30
   User: 
   Server: DEV-HQ
   Database: klv2db
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Extensions
	DROP CONSTRAINT FK_Extension_Language
GO
ALTER TABLE dbo.Languages SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Languages', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Languages', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Languages', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Extensions
	(
	ID bigint NOT NULL IDENTITY (1, 1),
	Name nvarchar(50) NOT NULL,
	LanguageID bigint NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Extensions SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_Extensions ON
GO
IF EXISTS(SELECT * FROM dbo.Extensions)
	 EXEC('INSERT INTO dbo.Tmp_Extensions (ID, Name, LanguageID)
		SELECT ID, CONVERT(nvarchar(50), Name), LanguageID FROM dbo.Extensions WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Extensions OFF
GO
DROP TABLE dbo.Extensions
GO
EXECUTE sp_rename N'dbo.Tmp_Extensions', N'Extensions', 'OBJECT' 
GO
ALTER TABLE dbo.Extensions ADD CONSTRAINT
	PK_Extension PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Extensions ADD CONSTRAINT
	UQ_Extension_Name UNIQUE NONCLUSTERED 
	(
	Name
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Extensions ADD CONSTRAINT
	FK_Extension_Language FOREIGN KEY
	(
	LanguageID
	) REFERENCES dbo.Languages
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Extensions', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Extensions', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Extensions', 'Object', 'CONTROL') as Contr_Per 