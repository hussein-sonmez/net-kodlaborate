USE [klv2db]
GO
/****** Object:  StoredProcedure [dbo].[sprocExecuteInContext]    Script Date: 12.2.2016 22:24:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sprocExecuteInContext]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sprocExecuteInContext]
GO
/****** Object:  StoredProcedure [dbo].[sprocErrorInfo]    Script Date: 12.2.2016 22:24:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sprocErrorInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sprocErrorInfo]
GO
/****** Object:  StoredProcedure [dbo].[sprocDDLTruncateExtension]    Script Date: 12.2.2016 22:24:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sprocDDLTruncateExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sprocDDLTruncateExtension]
GO
/****** Object:  StoredProcedure [dbo].[sprocCRUDUpdateExtension]    Script Date: 12.2.2016 22:24:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sprocCRUDUpdateExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sprocCRUDUpdateExtension]
GO
/****** Object:  StoredProcedure [dbo].[sprocCRUDRemoveExtension]    Script Date: 12.2.2016 22:24:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sprocCRUDRemoveExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sprocCRUDRemoveExtension]
GO
/****** Object:  StoredProcedure [dbo].[sprocCRUDAddExtension]    Script Date: 12.2.2016 22:24:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sprocCRUDAddExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sprocCRUDAddExtension]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Project_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[Project]'))
ALTER TABLE [dbo].[Project] DROP CONSTRAINT [FK_Project_User]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Line_Item]') AND parent_object_id = OBJECT_ID(N'[dbo].[Line]'))
ALTER TABLE [dbo].[Line] DROP CONSTRAINT [FK_Line_Item]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Item_Project]') AND parent_object_id = OBJECT_ID(N'[dbo].[Item]'))
ALTER TABLE [dbo].[Item] DROP CONSTRAINT [FK_Item_Project]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Item_Item]') AND parent_object_id = OBJECT_ID(N'[dbo].[Item]'))
ALTER TABLE [dbo].[Item] DROP CONSTRAINT [FK_Item_Item]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Comment_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[Comment]'))
ALTER TABLE [dbo].[Comment] DROP CONSTRAINT [FK_Comment_User]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Comment_Line]') AND parent_object_id = OBJECT_ID(N'[dbo].[Comment]'))
ALTER TABLE [dbo].[Comment] DROP CONSTRAINT [FK_Comment_Line]
GO
/****** Object:  Table [dbo].[User]    Script Date: 12.2.2016 22:24:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User]') AND type in (N'U'))
DROP TABLE [dbo].[User]
GO
/****** Object:  Table [dbo].[Project]    Script Date: 12.2.2016 22:24:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Project]') AND type in (N'U'))
DROP TABLE [dbo].[Project]
GO
/****** Object:  Table [dbo].[Line]    Script Date: 12.2.2016 22:24:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Line]') AND type in (N'U'))
DROP TABLE [dbo].[Line]
GO
/****** Object:  Table [dbo].[Item]    Script Date: 12.2.2016 22:24:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Item]') AND type in (N'U'))
DROP TABLE [dbo].[Item]
GO
/****** Object:  Table [dbo].[Extension]    Script Date: 12.2.2016 22:24:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Extension]') AND type in (N'U'))
DROP TABLE [dbo].[Extension]
GO
/****** Object:  Table [dbo].[Comment]    Script Date: 12.2.2016 22:24:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Comment]') AND type in (N'U'))
DROP TABLE [dbo].[Comment]
GO
/****** Object:  User [klv2admin]    Script Date: 12.2.2016 22:24:35 ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'klv2admin')
DROP USER [klv2admin]
GO
USE [master]
GO
/****** Object:  Database [klv2db]    Script Date: 12.2.2016 22:24:35 ******/
IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'klv2db')
DROP DATABASE [klv2db]
GO
/****** Object:  Database [klv2db]    Script Date: 12.2.2016 22:24:35 ******/
IF NOT EXISTS (SELECT name FROM sys.databases WHERE name = N'klv2db')
BEGIN
CREATE DATABASE [klv2db]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'klv2db', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\klv2db.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'klv2db_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\klv2db_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
END

GO
ALTER DATABASE [klv2db] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [klv2db].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [klv2db] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [klv2db] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [klv2db] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [klv2db] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [klv2db] SET ARITHABORT OFF 
GO
ALTER DATABASE [klv2db] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [klv2db] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [klv2db] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [klv2db] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [klv2db] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [klv2db] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [klv2db] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [klv2db] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [klv2db] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [klv2db] SET  DISABLE_BROKER 
GO
ALTER DATABASE [klv2db] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [klv2db] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [klv2db] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [klv2db] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [klv2db] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [klv2db] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [klv2db] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [klv2db] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [klv2db] SET  MULTI_USER 
GO
ALTER DATABASE [klv2db] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [klv2db] SET DB_CHAINING OFF 
GO
ALTER DATABASE [klv2db] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [klv2db] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [klv2db] SET DELAYED_DURABILITY = DISABLED 
GO
USE [klv2db]
GO
/****** Object:  User [klv2admin]    Script Date: 12.2.2016 22:24:35 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'klv2admin')
CREATE USER [klv2admin] FOR LOGIN [klv2admin] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [klv2admin]
GO
/****** Object:  Table [dbo].[Comment]    Script Date: 12.2.2016 22:24:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Comment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Comment](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[LineID] [bigint] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[State] [int] NOT NULL,
	[InsertedOn] [datetime] NOT NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_Comment] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[LineID] ASC,
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Extension]    Script Date: 12.2.2016 22:24:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Extension]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Extension](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nchar](32) NOT NULL,
	[MimeType] [nvarchar](96) NOT NULL,
 CONSTRAINT [PK_Extension] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_Extension_Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Item]    Script Date: 12.2.2016 22:24:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Item]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Item](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NOT NULL,
	[ExtensionID] [bigint] NOT NULL,
	[ProjectID] [bigint] NOT NULL,
	[ParentItemID] [bigint] NOT NULL,
	[InsertedOn] [datetime] NOT NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_Item] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Line]    Script Date: 12.2.2016 22:24:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Line]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Line](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[LineNumber] [int] NOT NULL,
	[Content] [nvarchar](255) NOT NULL,
	[ItemID] [bigint] NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[InsertedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_Line] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Project]    Script Date: 12.2.2016 22:24:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Project]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Project](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](255) NOT NULL,
	[UserID] [bigint] NOT NULL,
 CONSTRAINT [PK_Project] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[User]    Script Date: 12.2.2016 22:24:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[User](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Identifier] [nvarchar](255) NOT NULL,
	[FullName] [nvarchar](255) NOT NULL,
	[Cipher] [nchar](16) NOT NULL,
	[Token] [nchar](16) NULL,
	[State] [int] NOT NULL,
	[RegisteredOn] [datetime] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Comment_Line]') AND parent_object_id = OBJECT_ID(N'[dbo].[Comment]'))
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD  CONSTRAINT [FK_Comment_Line] FOREIGN KEY([LineID])
REFERENCES [dbo].[Line] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Comment_Line]') AND parent_object_id = OBJECT_ID(N'[dbo].[Comment]'))
ALTER TABLE [dbo].[Comment] CHECK CONSTRAINT [FK_Comment_Line]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Comment_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[Comment]'))
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD  CONSTRAINT [FK_Comment_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Comment_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[Comment]'))
ALTER TABLE [dbo].[Comment] CHECK CONSTRAINT [FK_Comment_User]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Item_Item]') AND parent_object_id = OBJECT_ID(N'[dbo].[Item]'))
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_Item] FOREIGN KEY([ParentItemID])
REFERENCES [dbo].[Item] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Item_Item]') AND parent_object_id = OBJECT_ID(N'[dbo].[Item]'))
ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_Item]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Item_Project]') AND parent_object_id = OBJECT_ID(N'[dbo].[Item]'))
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_Project] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[Project] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Item_Project]') AND parent_object_id = OBJECT_ID(N'[dbo].[Item]'))
ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_Project]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Line_Item]') AND parent_object_id = OBJECT_ID(N'[dbo].[Line]'))
ALTER TABLE [dbo].[Line]  WITH CHECK ADD  CONSTRAINT [FK_Line_Item] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Item] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Line_Item]') AND parent_object_id = OBJECT_ID(N'[dbo].[Line]'))
ALTER TABLE [dbo].[Line] CHECK CONSTRAINT [FK_Line_Item]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Project_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[Project]'))
ALTER TABLE [dbo].[Project]  WITH CHECK ADD  CONSTRAINT [FK_Project_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Project_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[Project]'))
ALTER TABLE [dbo].[Project] CHECK CONSTRAINT [FK_Project_User]
GO
/****** Object:  StoredProcedure [dbo].[sprocCRUDAddExtension]    Script Date: 12.2.2016 22:24:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sprocCRUDAddExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sprocCRUDAddExtension] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sprocCRUDAddExtension] 
	-- Add the parameters for the stored procedure here
	@Name nchar(32), 
	@MimeType nvarchar(96)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @command varchar(255);
	EXEC xp_sprintf @command OUTPUT, 
		'INSERT INTO [dbo].[Extension] VALUES (''%s'', ''%s'')', @Name, @MimeType
	EXEC [dbo].[sprocExecuteInContext]
		@SqlCommand = @command
	
END

GO
/****** Object:  StoredProcedure [dbo].[sprocCRUDRemoveExtension]    Script Date: 12.2.2016 22:24:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sprocCRUDRemoveExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sprocCRUDRemoveExtension] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sprocCRUDRemoveExtension] 
	-- Add the parameters for the stored procedure here
	@IDENTITY bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @command varchar(512);
	DECLARE @id varchar(24);
	SET @id = LTRIM(STR(@IDENTITY))
	EXEC xp_sprintf @command OUTPUT, 
		'DELETE FROM [dbo].[Extension] WHERE ID=%s', @id
	EXEC [dbo].[sprocExecuteInContext]
		@SqlCommand = @command
	
END

GO
/****** Object:  StoredProcedure [dbo].[sprocCRUDUpdateExtension]    Script Date: 12.2.2016 22:24:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sprocCRUDUpdateExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sprocCRUDUpdateExtension] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sprocCRUDUpdateExtension] 
	-- Add the parameters for the stored procedure here
	@IDENTITY bigint,
	@Name nvarchar(32), 
	@MimeType nvarchar(96)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @command varchar(512);
	DECLARE @id varchar(24);
	SET @id = LTRIM(STR(@IDENTITY))
	EXEC xp_sprintf @command OUTPUT, 
		'UPDATE [dbo].[Extension] SET Name=''%s'', MimeType=''%s'' WHERE ID=%s', @Name, @MimeType, @id
	EXEC [dbo].[sprocExecuteInContext]
		@SqlCommand = @command
END

GO
/****** Object:  StoredProcedure [dbo].[sprocDDLTruncateExtension]    Script Date: 12.2.2016 22:24:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sprocDDLTruncateExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sprocDDLTruncateExtension] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sprocDDLTruncateExtension] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @command varchar(512);

	EXEC [dbo].[sprocExecuteInContext]
		@SqlCommand = 'ALTER TABLE [dbo].[Item] DROP CONSTRAINT FK_Item_Extension;
		TRUNCATE TABLE [dbo].[Extension];
		ALTER TABLE [dbo].[Item] 
			ADD CONSTRAINT FK_Item_Extension FOREIGN KEY (ExtensionID) REFERENCES [dbo].[Extension] (ID)'
	
END

GO
/****** Object:  StoredProcedure [dbo].[sprocErrorInfo]    Script Date: 12.2.2016 22:24:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sprocErrorInfo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sprocErrorInfo] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sprocErrorInfo]
AS
BEGIN
	SELECT
		ERROR_NUMBER() AS ErrorNumber
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
END

GO
/****** Object:  StoredProcedure [dbo].[sprocExecuteInContext]    Script Date: 12.2.2016 22:24:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sprocExecuteInContext]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sprocExecuteInContext] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sprocExecuteInContext]
	@SqlCommand nvarchar(96)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		EXECUTE sp_executesql @SqlCommand
	END TRY
	BEGIN CATCH
		-- Execute error retrieval routine.
		EXECUTE [dbo].[sprocErrorInfo]
	END CATCH; 
END

GO
USE [master]
GO
ALTER DATABASE [klv2db] SET  READ_WRITE 
GO
