﻿define(['durandal/app', 'durandal/system', 'knockout', 'jquery', 'treeview'], function (app, system, ko, $, treeview) {

    ko.bindingHandlers.treeview = {
        init: function (element, valueAccessor) {
            var options = valueAccessor() || {};
            getjsonify(options.url, $(element), function (data) {
                $(element).treeview({
                        data: data,
                        onNodeSelected: function (event, data) {
                            var endpoint = 'items/' + data.itemID + '/lines';
                            getjsonify(endpoint, null, function (lines) {
                                console.log(lines);
                            });
                        }
                    });
            });
        },
        update: function (element, valueAccessor) {
            //ko.utils.unwrapObservable(valueAccessor());
        }
    };

    function ProjectAppViewModel() {

        var self = this;
        self.projectName = ko.observable("Project 1");

    }

    return ProjectAppViewModel;

});