﻿
transformUrl = function (url) {
    return 'http://192.168.1.12:177/' + url;
}

getjsonify = function (url, element, callback) {
    if (element) {
        $(element).append('Yükleniyor..');
        $(element).addClass("ui").addClass("text").addClass("loader");
        $(element).wrap('<div class="ui active dimmer"/>');
    }
    $.getJSON(transformUrl(url), function (data) {
        callback(data);
        if (element) {
            $(element).unwrap();
            $(element).removeClass("ui").removeClass("text").removeClass("loader");
        }
    });
}
postjsonify = function (url, data, element, callback) {
    if (element) {
        $(element).append('Yükleniyor..');
        $(element).addClass("ui").addClass("text").addClass("loader");
        $(element).wrap('<div class="ui active dimmer"/>');
    }
    $.postJSON(transformUrl(url), data, function (d) {
        callback(d);
        if (element) {
            $(element).unwrap();
            $(element).removeClass("ui").removeClass("text").removeClass("loader");
        }
    });
}