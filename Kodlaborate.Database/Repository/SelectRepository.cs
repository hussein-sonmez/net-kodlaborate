﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Kodlaborate.Database.Model;
using Kodlaborate.Protocol.Repository;

namespace Kodlaborate.Database.Repository {

    internal class SelectRepository<TEntity> : ISelectRepository<TEntity>
        where TEntity : class {
        public IEnumerable<TEntity> SelectBy(Expression<Func<TEntity, bool>> query, Action<StringBuilder> fallback = null) {

            return Selectify(ctx => {
                return ctx.Set<TEntity>().AsNoTracking().Where(query).ToList();
            }, fallback);

        }

        public IEnumerable<TEntity> Select(Action<StringBuilder> fallback = null) {

            return Selectify(ctx => {
                return ctx.Set<TEntity>().AsNoTracking().ToList();
            }, fallback);

        }

        public TEntity SelectById(long id, Action<StringBuilder> fallback = null) {

            return Selectify(ctx => {
                return ctx.Set<TEntity>().Find(id);
            }, fallback);

        }

        public async Task<TEntity> SelectByIdAsync(long id, Action<StringBuilder> fallback = null) {

            return await Selectify(async ctx => {
                return await ctx.Set<TEntity>().FindAsync(id);
            }, fallback);

        }

        public TEntity SelectSingleBy(Func<TEntity, bool> query, Action<StringBuilder> fallback = null) {

            return Selectify(ctx => {
                return ctx.Set<TEntity>().AsNoTracking().SingleOrDefault(query);
            }, fallback);

        }

        public async Task<TEntity> SelectSingleByAsync(Expression<Func<TEntity, bool>> query, Action<StringBuilder> fallback = null) {

            return await Selectify(async ctx => {
                return await ctx.Set<TEntity>().AsNoTracking().SingleOrDefaultAsync(query);
            }, fallback);

        }

        protected TResponse Contexify<TResponse>(Func<KLEntities, TResponse> commit, Action<StringBuilder> fallback = null) {
            DbContextTransaction tran = null;
            try {
                using (var ctx = new KLEntities()) {
                    using (tran = ctx.Database.BeginTransaction()) {
                        var resp = commit(ctx);
                        tran.Commit();
                        return resp;
                    }
                }
            } catch (DbEntityValidationException e) {
                if (fallback != null) {
                    StringBuilder sb = AssessValidation(e);
                    fallback(sb);
                }
                tran.Rollback();
                return default(TResponse);
            }
        }

        protected TResponse Selectify<TResponse>(Func<KLEntities, TResponse> commit, Action<StringBuilder> fallback = null) {
            try {
                var ctx = new KLEntities();
                ctx.Configuration.AutoDetectChangesEnabled = false;
                var resp = commit(ctx);
                ctx.Configuration.AutoDetectChangesEnabled = true;
                return resp;

            } catch (DbEntityValidationException e) {
                if (fallback != null) {
                    StringBuilder sb = AssessValidation(e);
                    fallback(sb);
                }
                return default(TResponse);
            }
        }

        protected TResponse Sprocify<TResponse>(Func<KLEntities, TResponse> commit, Action<StringBuilder> fallback = null) {
            try {
                using (var ctx = new KLEntities()) {
                    var resp = commit(ctx);
                    return resp;
                }
            } catch (DbEntityValidationException e) {
                if (fallback != null) {
                    StringBuilder sb = AssessValidation(e);
                    fallback(sb);
                }
                return default(TResponse);
            }
        }

        private static StringBuilder AssessValidation(DbEntityValidationException e) {
            var sb = new StringBuilder();
            foreach (var eve in e.EntityValidationErrors) {
                sb.AppendFormat("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                    eve.Entry.Entity.GetType().Name, eve.Entry.State);
                foreach (var ve in eve.ValidationErrors) {
                    sb.AppendFormat("- Property: \"{0}\", Error: \"{1}\"",
                        ve.PropertyName, ve.ErrorMessage);
                }
            }

            return sb;
        }
    }

}
