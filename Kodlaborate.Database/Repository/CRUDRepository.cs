﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kodlaborate.Database.Model;
using Kodlaborate.Protocol.Repository;

namespace Kodlaborate.Database.Repository {

    internal class CRUDRepository<TEntity> : SelectRepository<TEntity>, ICRUDRepository<TEntity>
        where TEntity : class {

        public long Insert(TEntity entity, Action<StringBuilder> fallback = null) {

            return Contexify(ctx => {
                ctx.Set<TEntity>().Add(entity);
                ctx.SaveChanges();
                var key = entity.GetType().GetProperties().Single(prop => prop.Name == "ID").GetValue(entity);
                return Convert.ToInt64(key);
            }, fallback);

        }

        public IEnumerable<TEntity> InsertRange(IEnumerable<TEntity> entities, Action<StringBuilder> fallback = null) {

            return Contexify(ctx => {
                var resp = ctx.Set<TEntity>().AddRange(entities);
                ctx.SaveChanges();
                return resp;
            }, fallback);

        }

        public long Update(TEntity entity, Action<StringBuilder> fallback = null) {

            return Contexify(ctx => {
                var key = entity.GetType().GetProperties().Single(prop => prop.Name == "ID").GetValue(entity);
                var old = base.SelectById(Convert.ToInt64(key), fallback);
                ctx.Entry(old).State = EntityState.Modified;
                ctx.SaveChanges();
                return Convert.ToInt64(key);
            }, fallback);

        }

        public long Delete(long id, Action<StringBuilder> fallback = null) {

            return Contexify(ctx => {
                ctx.Set<TEntity>().Remove(base.SelectById(id, fallback));
                ctx.SaveChanges();
                return id;
            }, fallback);

        }

        public async Task<long> InsertAsync(TEntity entity, Action<StringBuilder> fallback = null) {

            return await Contexify(async ctx => {
                ctx.Set<TEntity>().Add(entity);
                await ctx.SaveChangesAsync();
                var key = entity.GetType().GetProperties().Single(prop => prop.Name == "ID").GetValue(entity);
                return Convert.ToInt64(key);
            }, fallback);

        }

        public async Task<long> UpdateAsync(TEntity entity, Action<StringBuilder> fallback = null) {

            return await Contexify(async ctx => {
                var key = entity.GetType().GetProperties().Single(prop => prop.Name == "ID").GetValue(entity);
                var old = base.SelectById(Convert.ToInt64(key), fallback);
                ctx.Entry(old).State = EntityState.Modified;
                await ctx.SaveChangesAsync();
                return Convert.ToInt64(key);
            }, fallback);


        }

        public async Task<long> DeleteAsync(long id, Action<StringBuilder> fallback = null) {

            return await Contexify(async ctx => {
                ctx.Set<TEntity>().Remove(base.SelectById(id, fallback));
                await ctx.SaveChangesAsync();
                return id;
            }, fallback);

        }

        public ICRUDRepository<TEntity> Wipe(Action<StringBuilder> fallback = null) {

            return Contexify(ctx => {
                ctx.Set<TEntity>().RemoveRange(ctx.Set<TEntity>().ToList());
                ctx.SaveChanges();
                return this;
            }, fallback);

        }

        public async Task<ICRUDRepository<TEntity>> WipeAsync(Action<StringBuilder> fallback = null) {

            return await Contexify(async ctx => {
                ctx.Set<TEntity>().RemoveRange(ctx.Set<TEntity>().ToList());
                await ctx.SaveChangesAsync();
                return this;
            }, fallback);

        }

        public ICRUDRepository<TEntity> WipeDatabase(Action<StringBuilder> fallback = null) {

            return Sprocify(ctx => {
                ctx.sprocDDLTruncateDatabase();
                return this;
            }, fallback);

        }
    }

}
