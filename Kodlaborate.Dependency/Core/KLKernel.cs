﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kodlaborate.Database.Model;
using Kodlaborate.Database.Repository;
using Kodlaborate.Protocol.Repository;
using Kodlaborate.Protocol.Seed;
using Kodlaborate.Seed.Core;
using Ninject;

namespace Kodlaborate.Dependency.Core {


    public static class KLKernel {

        private static IKernel _Kernel;

        static KLKernel() {
            _Kernel = new StandardKernel();
            _Kernel.Bind(typeof(ISelectRepository<>)).To(typeof(SelectRepository<>));
            _Kernel.Bind(typeof(ICRUDRepository<>)).To(typeof(CRUDRepository<>));
            _Kernel.Bind(typeof(IDataSeed<Item>)).To(typeof(ItemSeed));
            _Kernel.Bind(typeof(IDataSeed<Language>)).To(typeof(LanguageSeed));
            _Kernel.Bind(typeof(IDataSeed<Line>)).To(typeof(LineSeed));
            _Kernel.Bind(typeof(IDataSeed<Project>)).To(typeof(ProjectSeed));
            _Kernel.Bind(typeof(IDataSeed<User>)).To(typeof(UserSeed));
            _Kernel.Bind(typeof(IDataSeed<Comment>)).To(typeof(CommentSeed));
        }

        public static T Get<T>() {
            return _Kernel.Get<T>();
        }

        public static IKernel Core() {
            return _Kernel;
        }

    }

}
